import {Cat} from "../data/categories.interface";
export class QuoteService{

  private addList: Cat[] =[];
  public addCheck:boolean = true;

  addAllQuoteToFavorite(items: Cat[]){
    this.addList.push(...items);
    this.addCheck =false;
  }

  updateQuoteFromFavorite(quote: Cat, check:number){

    const position = this.addList.find((quoteEl: Cat) =>{
      return quoteEl.id == quote.id;
    })
    if(position){
      if(check == 1)
        position.quantity++;
      else
        if(position.quantity>0)
          position.quantity--;
    }
  }

  deleteQuantity(quote: Cat){

    const position = this.addList.find((quoteEl: Cat) =>{
      return quoteEl.id == quote.id;
    })
    if(position){
       position.quantity =0;
    }
  }

  getFavoritesQuotes(){
    return this.addList.slice();
  }

  addChecking(){
    return this.addCheck;
  }
}
