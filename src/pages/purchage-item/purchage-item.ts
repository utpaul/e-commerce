import {Component, OnInit} from '@angular/core';
import {QuoteService} from "../../services/categoresList";
import {Cat} from "../../data/categories.interface";
@Component({
  selector: 'page-purchage-item',
  templateUrl: 'purchage-item.html'
})
export class PurchageItemPage implements OnInit{

  collection:Cat[];
  amount:number =0;

  constructor(private quoteService:QuoteService) {
  }
  ngOnInit(){
    this.collection = this.quoteService.getFavoritesQuotes();
    for(let coll of this.collection){
      this.amount += coll.quantity * coll.prize;
    }

    console.log(this.amount);
  }

}
