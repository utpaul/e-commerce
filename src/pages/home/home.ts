import {Component, OnInit} from '@angular/core';
import {Cat} from "../../data/categories.interface";
import items from "../../data/categories"
import {QuoteService} from "../../services/categoresList";
import {PurchageItemPage} from "../purchage-item/purchage-item";
import {AddedShoppingCardPage} from "../added-shopping-card/added-shopping-card";
import {PopoverController} from "ionic-angular";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{

  collection:Cat[];
  deleteCollection:Cat[];
  pItemPage=PurchageItemPage;
  constructor(private popoverCtrl: PopoverController,
              private quoteService:QuoteService){}

  ngOnInit(){
    if(this.quoteService.addChecking()){
      this.collection = items;
      this.quoteService.addAllQuoteToFavorite(this.collection);
    }
  }
  addCart(group:Cat){
    this.quoteService.updateQuoteFromFavorite(group, 1);
    this.collection=this.quoteService.getFavoritesQuotes();
  }

  removeCart(group:Cat){
    this.quoteService.updateQuoteFromFavorite(group, 2);
    this.collection=this.quoteService.getFavoritesQuotes();
  }


  onShowOptions(event: MouseEvent){

    const popover = this.popoverCtrl.create(AddedShoppingCardPage);
    popover.present({ev: event});

    popover.onDidDismiss(data =>{

      if (!data) {
        return;
      }else{
        this.quoteService.deleteQuantity( data.action);
        this.collection= this.quoteService.getFavoritesQuotes();
      }

    });
  }

}
