import { Component,OnInit } from '@angular/core';
import {ViewController} from "ionic-angular";
import {QuoteService} from "../../services/categoresList";
import {Cat} from "../../data/categories.interface";

@Component({
  selector: 'page-added-shopping-card',
  templateUrl: 'added-shopping-card.html'
})
export class AddedShoppingCardPage implements OnInit{


  collection:Cat[];
  constructor(private viewCtrl: ViewController,
              private quoteService:QuoteService){}

  ngOnInit(){
    this.collection = this.quoteService.getFavoritesQuotes();
    console.log(this.collection);
  }

  onAction(quoteGroup: Cat){
    this.viewCtrl.dismiss({action: quoteGroup});
  }
}
