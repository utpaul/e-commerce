import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {QuoteService} from "../services/categoresList";
import {PurchageItemPage} from "../pages/purchage-item/purchage-item";
import {AddedShoppingCardPage} from "../pages/added-shopping-card/added-shopping-card";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    PurchageItemPage,
    AddedShoppingCardPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    PurchageItemPage,
    AddedShoppingCardPage
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    QuoteService
  ]
})
export class AppModule {}
